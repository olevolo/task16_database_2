#4.1 select distinct maker from product where maker not in (select maker from product where model in (select model from laptop)) and maker in (select maker from product where model in (select model from pc));
#4.2 select distinct maker from product where maker = all (select maker from product where maker in (select maker from product where model in (select model from pc)) and maker not in (select maker from product where model in (select model from laptop)));
#4.3 select distinct maker from product where maker = any (select maker from product where model in (select model from pc)) and maker not in (select maker from product where model in(select model from laptop));

#5.1 select distinct maker from product where exists (select model from pc where product.model = pc.model);
#5.2 select distinct maker from product where exists (select model from pc where speed >= 750 and product.model = pc.model);
#5.4 select maker from product where exists (select model from pc where pc.model = product.model and speed >= all (select speed from pc));

#6.1 select concat('average price: ', cast(avg(price) as char)) as 'average price' from laptop;
#6.3 select concat(year(date),'.',month(date),'.',day(date)) as dates from income;
#6.5 select concat ('row: ', substring(place, 1, 1)) as seat_row, concat('seat: ', substring(place, 2, 1)) as seat_number from pass_in_trip;
#6.6 select town_from, town_to, concat ('from ',town_from,' to ',town_to) as destiny from trip;

#7.1 select model, price from printer having max(price);
#7.2 select model, speed from laptop having speed< all(select speed from pc);
#7.3 select maker, price from printer join product on printer.model = product.model where color = 'y' having price <any(select price from printer);
#7.6 select date, count(*) as 'count' from pass_in_trip where trip_no = any (select trip_no from trip where town_from = 'London') group by date;

#8.2 select maker, avg(screen) from product join laptop on product.model=laptop.model group by maker;
#8.3 select maker, max(price) from product join pc on product.model=pc.model group by maker;
#8.5 select speed, avg(price) from pc group by pc.speed having (speed>600)
#8.6 select maker, avg(hd) from product, pc where maker in (select distinct maker from product where  type = 'printer') and product.model = pc.model group by maker;

#9.3 select name, numGuns, bore, displacement, type, country, launched, s.class from ships s inner join Classes c on s.class = c.class
#where 
#case when numGuns = 8 then 1 else 0 end +
#case when bore = 15 then 1 else 0 end +
#case when displacement = 32000 then 1 else 0 end +
#case when type = 'bb' then 1 else 0 end +
#case when launched = 1915 then 1 else 0 end +
#case when s.class='Kongo' then 1 else 0 end +
#case when country='USA' then 1 else 0 end >= 4

#10.1
#select p.maker, pc.model, pc.price, p.type  from product p  join pc pc on pc.model = p.model and p.type = 'PC'  where p.maker = 'B' 
#union 
#select p.maker, l.model, l.price, p.type from product p join laptop l on l.model = p.model and p.type = 'Laptop'  where p.maker = 'B' 
#union 
#select p.maker, pr.model, pr.price, p.type from product p join printer pr on pr.model = p.model and p.type = 'Printer' where p.maker = 'B' ;

#10.3	
#select avg(price) as average from pc join product p on pc.model = p.model where p.maker = 'A'
#union
#select avg(price) as average from laptop join product p on laptop.model = p.model where p.maker = 'A';